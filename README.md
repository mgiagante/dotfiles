# My dotfiles

These dotfiles are ready to be applied and managed with GNU Stow.

Here's a guide on how to do it:
http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html

My OS is Manjaro-i3 Linux (a community edition).

## Additional software required

Tmux
----

To install, do:
```shell
$ sudo pacman -Sy tmux
```

Blueman
-------

```shell
$ sudo pacman -Sy blueman
```

Flameshot
---------

```shell
$ sudo pacman -Sy flameshot
```
