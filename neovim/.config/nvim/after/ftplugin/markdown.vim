"https://www.reddit.com/r/vim/comments/8asgjj/topnotch_vim_markdown_live_previews_with_no/
"https://github.com/dmarcotte/github-markdown-preview

function! OpenMarkdownPreview() abort
  if exists('s:markdown_job_id') && s:markdown_job_id > 0
    call jobstop(s:markdown_job_id)
    unlet s:markdown_job_id
  endif
  let available_port = system(
    \ "lsof -s tcp:listen -i :40500-40800 | awk -F ' *|:' '{ print $10 }' | sort -n | tail -n1"
    \ ) + 1
  if available_port == 1 | let available_port = 40500 | endif
  let s:markdown_job_id = jobstart('grip ' . shellescape(expand('%:p')) . ' :' . available_port)
  if s:markdown_job_id <= 0 | return | endif
  call system('open http://localhost:' . available_port)
endfunction
