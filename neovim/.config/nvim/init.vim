filetype plugin on

" Set tab key presses to produce 2 spaces instead.
set tabstop=2
set shiftwidth=2
set expandtab

" Enable relative and current line numbering.
set rnu
set nu

" Remove search highlighting with ESC ESC
nnoremap <esc><esc> :silent! nohls<cr>

" Load vim-plug plugins.
call plug#begin()
	Plug 'tpope/vim-rails'
	Plug 'scrooloose/syntastic'
	Plug 'tpope/vim-fugitive'
	Plug 'tpope/vim-rails
call plug#end()

" Defines Ruby syntax checkers for Syntastic.
let g:syntastic_ruby_checkers = ['mri', 'rubocop']

" Maps Control + b to paste a pry line when in insert mode.
autocmd FileType ruby imap <C-b> require "pry"; binding.pry<CR><ESC>
